using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SiteReports.Data;
using SiteReports.Models;

namespace SiteReports.Controllers
{
    public class ReportsController : Controller
    {
        private readonly SiteReportContext _context;

        public ReportsController(SiteReportContext context)
        {
            _context = context;    
        }

        // GET: Reports
        public async Task<IActionResult> Index()
        {
            return View(await _context.Reports.ToListAsync());
        }

        // GET: Reports/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var report = await _context.Reports
				.Include(r => r.StatementsOfWork)
	            .Include(r => r.Projects)
	            .Include(r => r.Risks)
	            .Include(r => r.Renewals)
				.SingleOrDefaultAsync(m => m.Id == id);
            if (report == null)
            {
                return NotFound();
            }

            return View(report);
        }

        // GET: Reports/Create
        public IActionResult Create(int siteId)
        {
			var reportModel = new Report();
	        reportModel.Site = _context.Sites.FirstOrDefault(x => x.Id == siteId);
	        reportModel.CreateDefaults(2);
			return View(reportModel);
        }

	    public ActionResult New(int siteId)
	    {
			var reportModel = new Report();
		    reportModel.Site = _context.Sites.FirstOrDefault(x => x.Id == siteId);
		    reportModel.CreateDefaults(2);
		    return View(reportModel);
		}

	    [HttpPost]
	    public ActionResult New(Report report)
	    {
		    report.Site = _context.Sites.FirstOrDefault(x => x.Id == report.SiteId);
			foreach(var statementOfWork in report.StatementsOfWork)
			{
				statementOfWork.SiteId = report.SiteId;
				statementOfWork.Site = report.Site;
			}
		    foreach (var project in report.Projects)
		    {
			    project.SiteId = report.SiteId;
			    project.Site = report.Site;
			}
		    foreach (var risk in report.Risks)
		    {
			    risk.SiteId = report.SiteId;
			    risk.Site = report.Site;
			}
		    foreach (var renewal in report.Renewals)
		    {
			    renewal.SiteId = report.SiteId;
			    renewal.Site = report.Site;
			}
			if (ModelState.IsValid)
		    {
			    //foreach (Phone phone in report.Phones.ToList())
			    //{
				   // if (phone.DeletePhone == true)
				   // {
					  //  // Delete Phone Numbers which is marked to remove
					  //  employee.Phones.Remove(phone);
				   // }
			    //}
			    _context.Reports.Add(report);
			    _context.SaveChanges();
		    }
		    return Redirect("Index");
	    }

		// POST: Reports/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ReportType,ReportDate,MonthlyMeetingSharePointUrl,ProactivePreventativeMaintenanceSharePointUrl,SiteManager,AsAtDate")] Report report)
        {
            if (ModelState.IsValid)
            {
                _context.Add(report);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(report);
        }

        // GET: Reports/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var report = await _context.Reports.SingleOrDefaultAsync(m => m.Id == id);
            if (report == null)
            {
                return NotFound();
            }
            return View(report);
        }

        // POST: Reports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ReportType,ReportDate,MonthlyMeetingSharePointUrl,ProactivePreventativeMaintenanceSharePointUrl,SiteManager,AsAtDate")] Report report)
        {
            if (id != report.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(report);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReportExists(report.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(report);
        }

        // GET: Reports/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var report = await _context.Reports
                .SingleOrDefaultAsync(m => m.Id == id);
            if (report == null)
            {
                return NotFound();
            }

            return View(report);
        }

        // POST: Reports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var report = await _context.Reports.SingleOrDefaultAsync(m => m.Id == id);
            _context.Reports.Remove(report);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool ReportExists(int id)
        {
            return _context.Reports.Any(e => e.Id == id);
        }
    }
}
