using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SiteReports.Data;
using SiteReports.Models;

namespace SiteReports.Controllers
{
    public class StatementOfWorksController : Controller
    {
        private readonly SiteReportContext _context;

        public StatementOfWorksController(SiteReportContext context)
        {
            _context = context;    
        }

        // GET: StatementOfWorks
        public async Task<IActionResult> Index()
        {
            var siteReportContext = _context.StatementOfWork.Include(s => s.Site);
            return View(await siteReportContext.ToListAsync());
        }

        // GET: StatementOfWorks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statementOfWork = await _context.StatementOfWork
                .Include(s => s.Site)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (statementOfWork == null)
            {
                return NotFound();
            }

            return View(statementOfWork);
        }

        // GET: StatementOfWorks/Create
        public IActionResult Create()
        {
            ViewData["SiteId"] = new SelectList(_context.Sites, "Id", "Id");
            return View();
        }

        // POST: StatementOfWorks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,StatementOfWorkId,Description,ClientSigned,ClientRejected,Value,SiteId")] StatementOfWork statementOfWork)
        {
            if (ModelState.IsValid)
            {
                _context.Add(statementOfWork);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["SiteId"] = new SelectList(_context.Sites, "Id", "Id", statementOfWork.SiteId);
            return View(statementOfWork);
        }

        // GET: StatementOfWorks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statementOfWork = await _context.StatementOfWork.SingleOrDefaultAsync(m => m.Id == id);
            if (statementOfWork == null)
            {
                return NotFound();
            }
            ViewData["SiteId"] = new SelectList(_context.Sites, "Id", "Id", statementOfWork.SiteId);
            return View(statementOfWork);
        }

        // POST: StatementOfWorks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,StatementOfWorkId,Description,ClientSigned,ClientRejected,Value,SiteId")] StatementOfWork statementOfWork)
        {
            if (id != statementOfWork.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(statementOfWork);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StatementOfWorkExists(statementOfWork.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["SiteId"] = new SelectList(_context.Sites, "Id", "Id", statementOfWork.SiteId);
            return View(statementOfWork);
        }

        // GET: StatementOfWorks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statementOfWork = await _context.StatementOfWork
                .Include(s => s.Site)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (statementOfWork == null)
            {
                return NotFound();
            }

            return View(statementOfWork);
        }

        // POST: StatementOfWorks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var statementOfWork = await _context.StatementOfWork.SingleOrDefaultAsync(m => m.Id == id);
            _context.StatementOfWork.Remove(statementOfWork);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool StatementOfWorkExists(int id)
        {
            return _context.StatementOfWork.Any(e => e.Id == id);
        }
    }
}
