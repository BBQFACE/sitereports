﻿using SiteReports.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteReports.Data
{
	public class DbInitializer
	{
		public static void Initialize(SiteReportContext context)
		{
			context.Database.EnsureCreated();

			// Look for any students.
			if (context.Sites.Any())
			{
				return;   // DB has been seeded
			}


			var sites = new Site[]
			{
			new Site{Name="Site Alpha",CreatedDate=DateTime.Parse("2005-09-21")},
			new Site{Name="Site Beta",CreatedDate=DateTime.Parse("2006-05-11")},
			new Site{Name="Site Charlie",CreatedDate=DateTime.Parse("2008-02-04")},
			new Site{Name="Site Delta",CreatedDate=DateTime.Parse("2009-06-22")},

			};
			foreach (Site s in sites)
			{
				context.Sites.Add(s);
			}
			context.SaveChanges();


			//var reports = new Report[]
			//{
			//	new Report
			//	{
			//		ReportType = ReportType.EngineerWeeklySiteReport,
			//		ReportDate = DateTime.Parse("2014-09-21"),
			//		MonthlyMeetingSharePointUrl = @"http://google.com",
			//		ProactivePreventativeMaintenanceSharePointUrl = @"http://google.com",
			//		SiteManager = "MC Butts",
			//		ProactivePreventativeMaintenance = new ProactivePreventativeMaintenance
			//		{
			//			Completed = false,
			//			DatePlan = DateTime.Parse("2014-10-10"),
			//			DateActual = DateTime.Parse("2014-10-13"),
			//			PercentComplete=40
			//		},
			//		StatementsOfWork = new List<StatementOfWork>()
			//		{	new StatementOfWork() {
			//				StatementOfWorkId = @"#ant-004",
			//				Description = @"New Surface Pro Elite Workbook for Margaret",
			//				ClientSigned = true,
			//				ClientRejected = false,
			//				Value = 2499.99m
			//			}
			//		},
			//		Site = sites[0],
			//		SiteVisit = new SiteVisit
			//		{
			//			TotalSiteVisitsThisWeek = 12,
			//			TotalSiteManagerVisitsThisWeek = 4,
			//		},
			//		Projects = new List<Project>()
			//		{
			//			new Project
			//			{
			//				Description = "Migrate all users from Exchange and offline-Office to Office 365",
			//				StartDate = DateTime.Parse("2017-02-21"),
			//				CompletedDate = DateTime.Parse("2017-02-26"),
			//				PercentComplete = 100
			//			},
			//			new Project
			//			{
			//				Description = "Assess client's network infrastructure & propose upgrade as per client suggestions",
			//				StartDate = DateTime.Parse("2017-02-21"),
			//				CompletedDate = DateTime.Parse("2017-02-26"),
			//				PercentComplete = 30
			//			}
			//		},
			//		Risks = new List<Risk>()
			//		{
			//			new Risk
			//			{
			//				Description = @"Business owner not satisfied with 'state of things lately'",
			//				Severity = RiskSeverity.High
			//			}
			//		}
			//	},

			//	// Add new report examples here, as above ^
			//};

			//foreach (Report r in reports)
			//{
			//	context.Reports.Add(r);
			//}
			//context.SaveChanges();

			

		}
    }
}
