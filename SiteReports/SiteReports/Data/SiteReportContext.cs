﻿using Microsoft.EntityFrameworkCore;
using SiteReports.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteReports.Data
{
    public class SiteReportContext : DbContext 
    {
		public SiteReportContext(DbContextOptions<SiteReportContext> options) : base(options)
		{

		}

		public DbSet<Site> Sites { get; set; }
		public DbSet<Report> Reports { get; set; }
		public DbSet<Project> Projects { get; set; }
		public DbSet<SiteVisit> SiteVisit { get; set; }
		public DbSet<StatementOfWork> StatementOfWork { get; set; }
		public DbSet<Risk> Risks { get; set; }
		public DbSet<ProactivePreventativeMaintenance> ProactivePreventativeMaintenance { get; set; }
		public DbSet<MonthlyMeeting> MonthlyMeeting { get; set; }
		
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Site>().ToTable("Site");
			modelBuilder.Entity<Report>().ToTable("Report");
		}
		
		public DbSet<SiteReports.Models.Renewal> Renewal { get; set; }

	}
}
