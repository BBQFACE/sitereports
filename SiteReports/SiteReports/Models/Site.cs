﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SiteReports.Models
{
    public class Site
    {
	    public int Id { get; set; }
	    public string Name { get; set; }
	    [DataType(DataType.Date)]
	    [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
		public DateTime CreatedDate { get; set; }
	    public ICollection<ProactivePreventativeMaintenance> ProactivePreventativeMaintenance { get; set; }
		public ICollection<Report> Reports { get; set; }
	    public ICollection<Project> Projects { get; set; }
	    public ICollection<StatementOfWork> StatementsOfWork { get; set; }
	    public ICollection<Risk> Risks { get; set; }
	}

	public class Report
	{
		public int Id { get; set; }
		[Required]
		[Display(Name = "Report Type")]
		public ReportType ReportType { get; set; }
		[Required]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
		public DateTime ReportDate { get; set; } = DateTime.Now;

		public string MonthlyMeetingSharePointUrl { get; set; }
		public string ProactivePreventativeMaintenanceSharePointUrl { get; set; }
		[Required]
		public string SiteManager { get; set; }

		public int SiteId { get; set; }
		public Site Site { get; set; }
		public SiteVisit SiteVisit { get; set; }
		public ProactivePreventativeMaintenance ProactivePreventativeMaintenance { get; set; }
		public ICollection<StatementOfWork> StatementsOfWork { get; set; }
		public ICollection<Project> Projects { get; set; }
		public ICollection<Risk> Risks { get; set; }
		public ICollection<Renewal> Renewals { get; set; }
		public DateTime AsAtDate { get; set; }

		internal void CreateDefaults(int count = 1)
		{
			StatementsOfWork = new List<StatementOfWork>();
			Projects = new List<Project>();
			Risks = new List<Risk>();
			Renewals = new List<Renewal>();
			AsAtDate = DateTime.Now;
			ReportDate = DateTime.Now;
			for (var i = 0; i < count; i++)
			{
				StatementsOfWork.Add(new StatementOfWork());
				Projects.Add(new Project());
				Risks.Add(new Risk());
				Renewals.Add(new Renewal());
			}
		}

	}

	public class Renewal
	{
		public int Id { get; set; }
		public string Description { get; set; }
		public int? Quantity { get; set; }
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? DueDate { get; set; }
		public Site Site { get; set; }
		public int SiteId { get; set; }
	}

	public enum ReportType
	{
		EngineerWeeklySiteReport = 1,
	}

	public class Project
	{
		public int Id { get; set; }
		public string Description { get; set; }
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? StartDate { get; set; }
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? CompletedDate { get; set; }
		[Range(0, 100)]
		public int? PercentComplete { get; set; }
		public Site Site { get; set; }
		public int SiteId { get; set; }
	}

	public class SiteVisit
	{
		public int Id { get; set; }
		public int? TotalSiteVisitsThisWeek { get; set; }
		public int? TotalSiteManagerVisitsThisWeek { get; set; }
		//public int SiteManagerVisitPercentage => (TotalSiteManagerVisitsThisWeek / (TotalSiteVisitsThisWeek == 0 ? 1 : TotalSiteVisitsThisWeek));
		//public int TechnicianVisitPercentage => (TotalSiteManagerVisitsThisWeek / TotalSiteVisitsThisWeek);
		[Required]
		public Site Site { get; set; }
		public int SiteId { get; set; }
	}

	public class StatementOfWork
	{
		public int Id { get; set; }
		public string StatementOfWorkId { get; set; }
		public string Description { get; set; }
		public bool ClientSigned { get; set; }
		public bool ClientRejected { get; set; }
		[Column(TypeName = "money")]
		public decimal Value { get; set; }
		public Site Site { get; set; }
		public int SiteId { get; set; }
	}

	public class Risk
	{
		public int Id { get; set; }
		public string Description { get; set; }
		public RiskSeverity Severity { get; set; }
		public Site Site { get; set; }
		public int SiteId { get; set; }
	}

	public enum RiskSeverity
	{
		Low,
		Medium,
		High,
		Severe
	}

	public class ProactivePreventativeMaintenance
	{
		public int Id { get; set; }
		public bool Completed { get; set; }
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? DatePlan { get; set; }
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? DateActual { get; set; }
		[Range(0, 100)]
		public int? PercentComplete { get; set; }
		public Site Site { get; set; }
		public int SiteId { get; set; }
	}

	public class MonthlyMeeting
	{
		public int Id { get; set; }
		public bool Completed { get; set; }
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? DatePlan { get; set; }
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? DateActual { get; set; }
		//public int PercentComplete { get; set; }
	}

}
